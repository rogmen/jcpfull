package cpfull;

import java.security.SecureRandom;

public class CPFull {
    private String cpf;
    //private boolean gerar;
    //private String errorMsg;
    private boolean status;
    private final int DEZ = 10;
    private final int ONZE = 11;
    
    public CPFull(){}
    public CPFull(String cpf){
        setCpf(cpf);
        cpfCheck(getCpf());
    }    
    
    //Algoritmo principal para confirmação da validade do CPF.
    public void cpfCheck(String cpf){
        setCpf(cpf);
        if((checarNumerosInvalidos()) && (getCpf().length() == 11)){
            //Chama o verificador de dígitos
            if((digCheck(DEZ)) && (digCheck(ONZE))){
                setStatus(true);
            }else{
                setStatus(false);
            }
        }        
    }
    
    //Algoritmo para checagem dos dígitos verificadores.
    private boolean digCheck(int multiplicador){
        int soma = 0;
        for(int i=multiplicador; i>=2; i--){
            soma += Character.getNumericValue(getCpf().charAt(multiplicador-i))*i;
        }

        if((((11-(soma%11)) < this.DEZ)) && ((11-(soma%11)) == Character.getNumericValue(getCpf().charAt(multiplicador-1)))){
            return true;
        } else if((((11-(soma%11)) > this.DEZ)) && (0 == Character.getNumericValue(getCpf().charAt(multiplicador-1)))){
            return true;
        } else{
            return false;
        }
    }
    
    private boolean checkNumber(){
        //Verifica se existe caracteres alfanuméricos na string.
        for(int i=0; i<11; i++){
            if (!Character.isDigit(cpf.charAt(i))){
                return false;
            }
        }
        return true;
    }
    
    //Não aceita números inválidos.
    private boolean checarNumerosInvalidos(){   
        for(int i=0; i<10; i++){
            String valor = "";
            for (int j=0; j<=10; j++){
                valor += Integer.toString(i);
            }
            if (valor.equals(getCpf())) {
                return false;
            }
        }
        return true;
        
//        switch(getCpf()){
//            case "00000000000" :
//            case "11111111111" :
//            case "22222222222" : 
//            case "33333333333" :
//            case "44444444444" :
//            case "55555555555" :
//            case "66666666666" :
//            case "77777777777" :
//            case "88888888888" :
//            case "99999999999" : return false;
//            default : return true;
//        }
    }
    
    //Gera números aleatórios com 9 dígitos.
    private String randomDigits(){
        SecureRandom random = new SecureRandom();
        String cpfRandom = Long.toString(Math.abs(random.nextInt(999999999)));
        return cpfRandom.substring(0, 9);
    }
    
    //Verifica região do CPF
    private String regiao(int nonoDigito){
        String regiao = "";
        switch(nonoDigito){
            case 1 : regiao = "DF, GO, MS, MT e TO"; break;
            case 2 : regiao = "AC, AM, AP, PA, RO e RR"; break;
            case 3 : regiao = "CE, MA e PI"; break;
            case 4 : regiao = "AL, PB, PE, RN"; break;
            case 5 : regiao = "BA e SE"; break;
            case 6 : regiao = "MG"; break;
            case 7 : regiao = "ES e RJ"; break;
            case 8 : regiao = "SP"; break;
            case 9 : regiao = "PR e SC"; break;
            case 0 : regiao = "RS"; break;
        }
        return regiao;
    }

 //
 // Getters and Setters
 //
    public void setCpf(String cpf){
        this.cpf = cpf;
    }

    public String getCpf(){
        return this.cpf;
    }
    
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }    

}


